var globalData = [];
var watched = [];
var isAll = true;
function getDetailCourses() {
    var pathArray = window.location.pathname.split('/');
    var courseId = pathArray[pathArray.length - 2];
    var courseContentUrl = "https://app.pluralsight.com/learner/content/courses/" + courseId;
    var exercisesFileUrl = "https://app.pluralsight.com/learner/courses/" + courseId + "/exercise-files";

    $.ajax(courseContentUrl, {
        dataType: "json",
        contentType: "application/json"}).done(function (data) {
        var listModules = data.modules;
        var courseName = data.title;
        $.ajax(exercisesFileUrl, {dataType: "json", contentType: "application/json"}).done(function (data) {
            globalData.push({clipUrl: data.exerciseFilesUrl, moduleName: "", clipName: "exercise-file.zip", courseName: courseName});
        });
        var interval = 1000;
        for (var i = 0; i < listModules.length; i++) {
            interval = fetchVideo(listModules[i], interval, i, courseName);
        }

        setTimeout(getRestCourses, interval);
    });
}

function downloadFile() {
    var content = "";
    if (globalData.length > 0) {
        for (var i = 0; i < globalData.length; i++) {
            var courseName = globalData[i].courseName
            content += "idman /d " + globalData[i].clipUrl + " /f \"Pluralsight - " + courseName.replace(/[^a-zA-Z0-9 .-]/g, "").trim() + "\\";
            if (globalData[i].moduleName != "") {
                var moduleName = globalData[i].moduleName;
                content += moduleName.replace(/[^a-zA-Z0-9 .-]/g, "").trim() + "\\";
            }
            var clipName = globalData[i].clipName;
            content += clipName.replace(/[^a-zA-Z0-9 .-]/g, "").trim() + "\" /a\n";
        }
        var uriContent = "data:application/octet-stream," + encodeURIComponent(content);
        window.open(uriContent, 'new');
    } else {
        alert("Content Tidak Ada");
    }
}

function fetchVideo(module, interval, index, courseName) {
    var moduleName = index + ". " + module.title;
    var moduleId = module.id;
    var moduleId = moduleId.split('|');
    var clips = module.clips;
    for (var j = 0; j < clips.length; j++) {
        var jsonData = {author: moduleId[1]
            , clipIndex: j
            , courseName: moduleId[0]
            , includeCaptions: false
            , locale: "en"
            , mediaType: "mp4"
            , moduleName: moduleId[2]
            , quality: "848x640"};
        if (watched.includes(clips[j].id) == false) {
            setTimeoutWithClosure(jsonData, interval, module, index, clips[j], j, courseName);
            interval = interval + 1000;
        }
    }

    return interval;

}

function setTimeoutWithClosure(jsonData, interval, module, index, clip, clipIndex, courseName) {
    var moduleName = index + ". " + module.title;
    var clipName = clipIndex + ". " + clip.title + ".mp4";
    var videoUrl = "https://app.pluralsight.com/video/clips/viewclip";
    setTimeout(function () {
        $.ajax(videoUrl, {
            type: "POST",
            dataType: "json",
            data: JSON.stringify(jsonData),
            contentType: "application/json"
        }).done(function (data) {
            var clipUrl = data.urls[0].url;
            globalData.push({clipUrl: clipUrl, moduleName: moduleName, clipName: clipName, courseName: courseName});
        }).fail(function (jqXHR, textStatus) {
            if (textStatus == "error") {
                isAll = false;
                interval = interval + 1000;
                setTimeoutWithClosure(jsonData, interval, module, index, clip, clipIndex, courseName);
            }
        });
    }, interval);
}

function getRestCourses() {
    var pathArray = window.location.pathname.split('/');
    var courseId = pathArray[pathArray.length - 2];
    var courseContentUrl = "https://app.pluralsight.com/learner/content/courses/" + courseId;
    var watchedContentUrl = "https://app.pluralsight.com/learner/user/courses/" + courseId + "/progress";

    $.ajax(watchedContentUrl, {
        dataType: "json",
        contentType: "application/json"}).done(function (data) {
        watched = data.watchedContentIds;
        $.ajax(courseContentUrl, {
            dataType: "json",
            contentType: "application/json"}).done(function (data) {
            var listModules = data.modules;
            var courseName = data.title;
            var interval = 1000;
            for (var i = 0; i < listModules.length; i++) {
                interval = fetchVideo(listModules[i], interval, i, courseName);
            }
            setTimeout(downloadFile, interval);
        });
    });

}

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    switch (msg.message) {
        case "download":
            getDetailCourses();
            break;
        case "get-rest":
            getRestCourses();
            break;
        default:
            break;
    }
    sendResponse();
});