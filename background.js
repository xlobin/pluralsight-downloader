var showForPages = ["*://app.pluralsight.com/*"];

chrome.contextMenus.create({
    "id": "pludol-context-menu",
    "title": "Download Video",
    "documentUrlPatterns": showForPages
});

chrome.contextMenus.create({
    "id": "pludol-the-rest-view-menu",
    "title": "Get The Rest Video",
    "documentUrlPatterns": showForPages
});


function genericOnClick(info, tab) {
    var message = {"message": "download"}
    if (info.menuItemId === "pludol-the-rest-view-menu") {
        message = {"message": "get-rest"};
    }
    chrome.tabs.sendMessage(tab.id, message);
}

chrome.contextMenus.onClicked.addListener(genericOnClick);